# My boilerplate for Laravel, Vue applications with JWT auth and material components

This is a WIP, but it's my usable boilerplate for Laravel/Vue applications with JWT auth and Material Design components. Not sure if it is of any value to anyone other than myself, but I didn't want to have to keep reimplementing things on new projects so I made this. It could definitely use some improvements so if you use it and make any improvements to it, please make a PR for it!

## Things that are working
  * Vue router
  * JWT Auth
  * Material Design for App layout

## Things that need to be improved
  * This documentation
  * Clean up registration and login forms so they don't look like shit
